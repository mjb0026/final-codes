#include "DATALOG.h"
#include "Arduino.h"
Datalog::Datalog() {}
Datalog::~Datalog() {}

bool Datalog::CheckFile(fs::FS &fs, const String & path) {
  File file = fs.open(path);
  if (file) {
    file.close();
    return true;
  }
  else {
    return false;
  }
}

void Datalog::AppendFile(fs::FS &fs, const String & path, const String & message, double Impedance[21], bool isImp) {
  if (!CheckFile(fs, path)) {
    WriteFile(fs, path, isImp);
  }
  Serial.println("Datalog file: " + path + " Appended");
  File file = fs.open(path, FILE_APPEND);
  String imp;
  if (isImp) {
    for (int i; i < 21; i++) {
      imp += String(Impedance[i]) + "\t";
    }
  }
  else {
    for (int i; i < 21; i++) {
      imp += String(Impedance[i],6) + "\t";
    }
  }
  String temp = message + "\t\t" + imp + "\n";
  file.print(temp);
  file.close();
}
void Datalog::WriteFile(fs::FS &fs, const String & path, bool isImp) {
  File file = fs.open(path, FILE_WRITE);
  Serial.println("Datalog file: " + path + " Created");
  if (isImp) {
    file.print("Time\tImpedance @:\t3000Hz\t\t3010Hz\t\t3020Hz\t\t3030Hz\t\t3040Hz\t\t3050Hz\t\t3060Hz\t\t3070Hz\t\t3080Hz\t\t3090Hz\t\t3100Hz\t\t3110Hz\t\t3120Hz\t\t3130Hz\t\t3140Hz\t\t3150Hz\t\t3160Hz\t\t3170Hz\t\t3180Hz\t\t3190Hz\t\t3200Hz\n");
    file.print("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
    file.close();
  }
  else {
    file.print("Time\t    Phase @:\t3000Hz\t\t3010Hz\t\t3020Hz\t\t3030Hz\t\t3040Hz\t\t3050Hz\t\t3060Hz\t\t3070Hz\t\t3080Hz\t\t3090Hz\t\t3100Hz\t\t3110Hz\t\t3120Hz\t\t3130Hz\t\t3140Hz\t\t3150Hz\t\t3160Hz\t\t3170Hz\t\t3180Hz\t\t3190Hz\t\t3200Hz\n");
    file.print("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
    file.close();
  }
}