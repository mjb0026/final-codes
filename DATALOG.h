#ifndef DATALOG_H
#define DATALOG_H
#include "Arduino.h"
#include "FS.h"
#include <SD.h>
#include <SPI.h>


class Datalog
{
  public:
    Datalog();
    bool CheckFile(fs::FS &fs, const String & path);
    void AppendFile(fs::FS &fs, const String & path, const String & message, double Impedance[21],bool isImp);
    void WriteFile(fs::FS &fs, const String & path,bool isImp);
    ~Datalog();
};

#endif