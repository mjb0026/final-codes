#ifndef PMOD_H
#define PMOD_H
#include "Arduino.h"
#include <Wire.h>
#include <Math.h>
/*
   Note: The PMOD IA follows a Big Endian order, what the highest order byte that contains bits
   8- 15 come first. The Register map can be found on page 23 of the AD5933 (rev. F) document

   An example is given below for the control register:

   Register |  Name   | Register data
   --------------------------------
   0x80     | Control | D15 to D8
   0x81     | Control | D7 to  D0
*/

#define PMOD_DEBUG_OUTPUT     false          //Verbose mode for debugging
#define PMOD_RAW_DATA_OUTPUT  false          //Output raw Pmod real/imag data

// Contains the defined register addresses
#define Bus                   (0x0D)        //PMOD IA default serial bus address (What you use to begin transmission)
#define Addr_ptr              (0xB0)        //allows user to set the address pointer to any location in memory
#define Status_reg            (0x8F)        //Status register 
#define Ctrl_reg              (0x80)        //Starting address for the control register
#define Real_data_reg         (0x94)        //Output of DSP for the real part of impedance
#define Imag_data_reg         (0x96)        //Output of DSP for the imag part of impedance

// Defined values used to change the control register bits d15-12 to change the mode of operation

#define Init_with_start_freq  (0x10)        //Initialize with start frequency
#define Start_freq_sweep      (0x20)        //Start Frequency sweep
#define Inc_freq              (0x30)        //increment Frequency 
#define Rep_freq              (0x40)        //repeat frequency
#define Measure_temp          (0x90)        //Measure Temperature
#define Power_down            (0xA0)        //Power-down mode
#define Standby               (0xB0)        //standby mode

// Bit masks for better code clarity - D7 is MSB of a byte, D0 is LSB
#define D0        0x01
#define D1        0x02
#define D2        0x04
#define D3        0x08
#define D4        0x10
#define D5        0x20
#define D6        0x40
#define D7        0x80

// Additional configuration values
#define MEAS_DELAY_CYCLES       511         //Number of clock cycles to delay (max 511)
#define MEAS_DELAY_MULTIPLIER   4           //Multiplier for clock cycle delay (1, 2, or 4)
#define PGA_GAIN                1           //Amplifier gain (0 = x5, 1 = x1)

#define I2C_ATTEMPTS            10          //Number of attempts to perform for I2C communication operations
// Should be >= 1

class Pmod {

  public:
    Pmod();
    ~Pmod();
    byte ReadByte(byte Register);
    byte ReadStatusRegister();
    long ReadControlRegister();
    void Reset();
    void SetMode(byte IAMode);
    void SetSweepParameters(byte excit_voltage, long calibration_freq, long freq_increment,
                            int num_incr);
    void GetSweepParameters(byte& excit_voltage, long& calibration_freq, long& freq_increment,
                            int& num_incr);
    void Calibrate(float *Real_data, float *Imag_data, double *Gain_factors,
                   byte excit_voltage, long calibration_freq, long freq_increment,
                   int num_incr, double known_impedance);
    void DoFreqSweep(float *real_data, float *imag_data);
    void GetImpedances(float *real_data, float *imag_data, long *frequencies,
                       double *impedance_mag, double *impedance_phase, int num_vals);


  private:
    bool SetByte(byte Addr, byte Memory);
    bool GetByte(byte Addr, byte *ptr);
    long ReadRegister(byte Starting_addr);
    void SetStartFreq(long calibration_freq);
    void SetFreqIncrement(long freq_incr);
    void SetNumIncrements(int num_incr);
    //void SetGainFactor(double real_data, double imag_data, double known_impedance, bool set_low = true);
    void SetGainFactors(float *real_data, float *imag_data, double known_impedance);
    void SetSystemPhase(double real_data, double imag_data, bool set_low = true);
    double mapDouble(double val, double fromL, double fromH, double toL, double toH);

    const unsigned long Internal_clock = 16776000;    //Frequency of the Pmod clock
    byte Excit_voltage = 0xFF;                        //Byte code selecting excitation voltage
    long Sweep_calibration_freq = 0;                  //Sweep start freq (Hz)
    long Sweep_freq_increment = 0;                    //Sweep increment freq (Hz)
    int Sweep_num_increments = 0;                     //Sweep increments (num iterations less one)
    //double Gain_factor_lowf = 0;                      //Gain factor for measured data at start of freq range
    //double Gain_factor_highf = 0;                     //Gain factor for measured data at end of freq range
    double *Gain_factor_buf = NULL;                   //Array of gain factors for each element in frequency range
    double System_phase_lowf = 0;                     //Internal phase of system (radians) at low end of freq sweep,
    //  set when calibrating and subtracted from phase measurements
    double System_phase_highf = 0;                    //Internabl phase of system at high end of freq sweep
};

#endif