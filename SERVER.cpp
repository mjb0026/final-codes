#include "SERVER.h"
#include "Arduino.h"

Srvr::Srvr(const int port) {
  server = new ESP32WebServer(port);
  wifiMulti = new WiFiMulti;
}

void Srvr::RunServer() {
  server->handleClient();
}

void Srvr::Home() {
  File root = SD.open("/");
  if (root) {
    root.rewindDirectory();
    SendHTML_Header();
    webpage += F("<table align='center'>");
    webpage += F("<tr><th>Files</th></tr>");
    printDirectory("/", 0);
    webpage += F("<tr><th>------------------------------------------------------------------------------</th></tr>");
    webpage += F("<tr><th><a href='/download'><button>Download Tab</button></a></th></tr>");
    webpage += F("</table>");
    SendHTML_Content();
    root.close();
  }
  else
  {
    SendHTML_Header();
    webpage += F("<h3>No Files Found</h3>");
  }
  SendHTML_Content();
  SendHTML_Stop();   // Stop is needed because no content length was sent
}

void Srvr::File_Download() { // This gets called twice, the first pass selects the input, the second pass then processes the command line arguments
  if (server->args() > 0 ) { // Arguments were received
    if (server->hasArg("download")) SD_file_download(server->arg(0));
  }
  else SelectInput("Enter filename <br>then press download", "download", "download");
}

void Srvr::printDirectory(const char * dirname, uint8_t levels) {
  File root = SD.open(dirname);
  if (!root) {
    return;
  }
  if (!root.isDirectory()) {
    return;
  }
  File file = root.openNextFile();
  while (file) {
    if (webpage.length() > 1000) {
      SendHTML_Content();
    }
    if (file.isDirectory()) {
      webpage += "<tr><td>" + String(file.name()) + "</td></tr>";
      printDirectory(file.name(), levels - 1);
    }
    else
    {
      webpage += "<tr><td>" + String(file.name()) + "</td>";
      webpage += "<td>"  "</td>";
    }
    file = root.openNextFile();
  }
  file.close();
}
void Srvr::SelectInput(String heading1, String command, String arg_calling_name) {
  SendHTML_Header();
  webpage += F("<table align='center'>");
  webpage += F("<tr align='center'><td>");
  webpage += F("<h3>"); webpage += heading1 + "</h3>";
  webpage += F("<p><strong>Example:</strong> 1_1_20.txt </p>");
  webpage  += F("</td></tr>");
  webpage += F("<tr align='center'><td>");
  webpage += F("<FORM action='/"); webpage += command + "' method='post'>"; // Must match the calling argument e.g. '/chart' calls '/chart' after selection but with arguments!
  webpage += F("<input type='text' name='"); webpage += arg_calling_name; webpage += F("' value=''><br>");
  webpage  += F("</td></tr>");
  webpage += F("<tr align='center'><td>");
  webpage += F("<input type='submit' name='"); webpage += arg_calling_name; webpage += F("' value='Download'><br><br>");
  webpage += F("<input type='button' value='Go back' onclick='history.back()'>");
  webpage  += F("</td></tr>");
  webpage += F("</table>");
  SendHTML_Content();
  SendHTML_Stop();
}
void Srvr::Configure(const int IP[], const int Gateway[], const int Subnet[], const int Dns[]) {
  local_IP = new IPAddress(IP[0], IP[1], IP[2], IP[3]);
  gateway = new IPAddress(Gateway[0], Gateway[1], Gateway[2], Gateway[3]);
  subnet = new IPAddress(Subnet[0], Subnet[1], Subnet[2], Subnet[3]);
  dns = new IPAddress(Dns[0], Dns[1], Dns[2], Dns[3]);
  if (!WiFi.config(*local_IP, *gateway, *subnet, *dns)) {
    Serial.println("Failed to configure Correctly");
  }
}

void Srvr::Connect(const String ssid, const String pass) {
  wifiMulti ->addAP(ssid.c_str(), pass.c_str());
  while (wifiMulti->run() != WL_CONNECTED);
  Serial.println("Connected @ " + WiFi.localIP().toString());
  server->on("/", [&] () {
    Home();
  });
  server->on("/download", [&] () {
    File_Download();
  });
  server->begin();


}

void Srvr::SendHTML_Header() {
  server->sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  server->sendHeader("Pragma", "no-cache");
  server->sendHeader("Expires", "-1");
  server->setContentLength(CONTENT_LENGTH_UNKNOWN);
  server->send(200, "text/html", "");
  webpage  = F("<!DOCTYPE html><html>");
  webpage += F("<head>");
  webpage += F("<title>WIM</title>");
  server->sendContent(webpage);
  webpage = "";
}

void Srvr::SendHTML_Content() {
  server->sendContent(webpage);
  webpage = "";
}

void Srvr::SendHTML_Stop() {
  server->sendContent("");
  server->client().stop(); // Stop is needed because no content length was sent
}

void Srvr::ReportFileNotPresent(String target) {
  SendHTML_Header();
  webpage += F("<h3>File does not exist</h3>");
  webpage += F("<a href='/"); webpage += target + "'>[Back]</a><br><br>";
  SendHTML_Content();
  SendHTML_Stop();
}


void Srvr::SD_file_download(String filename) {
  File download = SD.open("/" + filename);
  if (download) {
    server->sendHeader("Content-Type", "text/text");
    server->sendHeader("Content-Disposition", "attachment; filename=" + filename);
    server->sendHeader("Connection", "close");
    server->streamFile(download, "application/octet-stream");
    download.close();
  } else ReportFileNotPresent("download");
}

Srvr::~Srvr() {
  delete server;
  delete wifiMulti;
  delete local_IP;
  delete gateway;
  delete subnet;
  delete dns;
}