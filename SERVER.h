#ifndef SERVER_H
#define SERVER_H
#include "Arduino.h"
#include <WiFi.h>
#include <WiFiMulti.h>
#include <ESP32WebServer.h>             //https://github.com/Pedroalbuquerque/ESP32WebServer
#include <ESPmDNS.h>
#include "FS.h"
#include <SD.h>
#include <SPI.h>

class Srvr
{
  public:
    Srvr(const int port);
    void Configure(const int IP[], const int Gateway[], const int Subnet[], const int Dns[]);
    void Connect(const String ssid, const String pass);
    void RunServer();
    ~Srvr();

  private:
    ESP32WebServer *server;
    WiFiMulti *wifiMulti;
    IPAddress *local_IP;
    IPAddress *gateway;
    IPAddress *subnet;
    IPAddress *dns;
    String h = "/";
    String d = "/download";
    String webpage = "";
    void File_Download();
    void Home();
    void SendHTML_Header();
    void SendHTML_Content();
    void SendHTML_Stop();
    void ReportFileNotPresent(String target);
    void SelectInput(String heading1, String command, String arg_calling_name);
    void SD_file_download(String filename);
    void printDirectory(const char * dirname, uint8_t levels);

};

#endif