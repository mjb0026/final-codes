#include "Arduino.h"
#include "DATALOG.h"
#include "SERVER.h"
#include "PMOD.h"
#include <WiFi.h>
#include "NTPClient.h"   //https://github.com/taranais/NTPClient
#include <WiFiUdp.h>
#include <Wire.h>
#include <SPI.h>
#include <math.h>

//start user defined parameters
#define EXCIT_VOLTAGE     3                               //refer to page 23-24 of AD5993 DATA sheet
#define CALIBRATION_FREQ  3000                            //refer to page 23-24 of AD5993 DATA sheet
#define FREQ_INCREMENT    10                              //refer to page 23-24 of AD5993 DATA sheet
#define NUM_INCREMENTS    20                              //refer to page 23-24 of AD5993 DATA sheet
#define CALIBRATION_IMPED 10e4                            //What calibration resitantce you are using 
unsigned long  DatalogTimer = 6 * pow(10, 7);            //How often you want to record time, its in microseconds
#define GMT_Off          -21600                           //gmt offset to your local time zone, its in seconds  
const String ssid = "ATT5KVttvS";                         //Your SSID
const String pass = "brunickbunch17";                     //Your Pass
const byte port      = 80;
const bool  Static_IP = false;                             //pick if you want it to get its own IP, or If you want to define a static IP.
//if Static_IP = true, then set the IP, Gateway, Subnet, and DNS. if Static_ip = false, leave as is.
const int  IP[]      = {1, 1, 1, 1};                //ip address you want
const int  Gateway[] = {1, 1, 1, 1};                //Your gateway
const int  Subnet[]  = {1, 1, 1, 1};                 //your subnet
const int  Dns[]     = {1, 1, 1, 1};                 //your dns
//end user defined parameters

#define I2C_FREQUENCY     100000                          //100 kHz
#define SDA 21
#define SCL 22
float measuredRealBuf[NUM_INCREMENTS + 1];
float measuredImagBuf[NUM_INCREMENTS + 1];
long frequencies[NUM_INCREMENTS + 1];
double gainFactorBuf[NUM_INCREMENTS + 1] = {0};
double measuredImpedMag[NUM_INCREMENTS + 1];
double measuredImpedPhase[NUM_INCREMENTS + 1];
Pmod IA;

//Interrupt
volatile int interruptCounter;
hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;
int totalInterruptCounter;
void IRAM_ATTR onTimer() {
  portENTER_CRITICAL_ISR(&timerMux);
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
}

// Server
Srvr MyServer(port);

// Datalog
#define SD_CS_pin     5
Datalog MyDatalog;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
String formattedDate;
String dayStamp;
String timeStamp;

void setup() {
  Serial.begin(9600);
  Wire.begin();
  while (!Serial);
  Wire.setClock(I2C_FREQUENCY);
  if (Static_IP == true) {
    MyServer.Configure(IP, Gateway, Subnet, Dns);
  }
  MyServer.Connect(ssid, pass);
  pinMode(19, INPUT_PULLUP);                      //Internal resistor pull-up for ESP32 SPI
  SD.begin(SD_CS_pin);
  timeClient.begin();
  timeClient.setTimeOffset(GMT_Off);               //gmt offest in milliseconds
  while (!timeClient.update()) {
    timeClient.forceUpdate();
  }
  Serial.print(F("Calibrating...  (resistance = "));
  Serial.print(CALIBRATION_IMPED); Serial.println(F(")"));
  IA.Calibrate(measuredRealBuf, measuredImagBuf, gainFactorBuf, EXCIT_VOLTAGE, CALIBRATION_FREQ, FREQ_INCREMENT, NUM_INCREMENTS, CALIBRATION_IMPED);
  Serial.println(F("Calibration Done.\n"));
  Serial.println("Initial sweep");
  int i;
  IA.SetSweepParameters(EXCIT_VOLTAGE, CALIBRATION_FREQ, FREQ_INCREMENT, NUM_INCREMENTS);
  IA.DoFreqSweep(measuredRealBuf, measuredImagBuf);
  for (i = 0; i <= NUM_INCREMENTS; i++) {
    frequencies[i] = CALIBRATION_FREQ + FREQ_INCREMENT * i;
  }
  IA.GetImpedances(measuredRealBuf, measuredImagBuf, frequencies, measuredImpedMag, measuredImpedPhase, NUM_INCREMENTS + 1);
  Serial.println(F("Impedance"));
  for (i = 0; i < NUM_INCREMENTS + 1; i++) {
    Serial.print(measuredImpedMag[i]);
    Serial.print("\t");
    Serial.println(measuredImpedPhase[i], 6);
  }
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, DatalogTimer, true);
  timerAlarmEnable(timer);
  Serial.println("Entering main loop");
}

void loop() {

  MyServer.RunServer();
  if (interruptCounter > 0) {
    portENTER_CRITICAL(&timerMux);
    interruptCounter--;
    portEXIT_CRITICAL(&timerMux);
    int i;
    // Do full sweep as a test
    Serial.println(F("Starting sweep."));
    IA.SetSweepParameters(EXCIT_VOLTAGE, CALIBRATION_FREQ, FREQ_INCREMENT, NUM_INCREMENTS);
    IA.DoFreqSweep(measuredRealBuf, measuredImagBuf);
    for (i = 0; i <= NUM_INCREMENTS; i++) {
      frequencies[i] = CALIBRATION_FREQ + FREQ_INCREMENT * i;
    }
    IA.GetImpedances(measuredRealBuf, measuredImagBuf, frequencies, measuredImpedMag, measuredImpedPhase, NUM_INCREMENTS + 1);
    Serial.println(F("Impedance\tPhase"));
    for (i = 0; i < NUM_INCREMENTS + 1; i++) {
      Serial.print(measuredImpedMag[i]);
      Serial.print("\t");
      Serial.println(measuredImpedPhase[i], 6);
    }
    String T = GetFileTime();
    MyDatalog.AppendFile(SD, GetFileNameIMP(), T, measuredImpedMag, true);
    MyDatalog.AppendFile(SD, GetFileNamePhase(), T, measuredImpedPhase, false);
    Serial.flush();
    delay(10000);
  }
}

String GetFileNameIMP() {
  while (!timeClient.update()) {
    timeClient.forceUpdate();
  }
  formattedDate = timeClient.getFormattedDate();
  String Year = formattedDate.substring(0, 2);
  String Month = formattedDate.substring(5, 7);
  String Day = formattedDate.substring(8, 10);
  String FileName = "/" + Month + "_" + Day + "_" + Year + "IMP.txt";
  return FileName;
}
String GetFileNamePhase() {
  while (!timeClient.update()) {
    timeClient.forceUpdate();
  }
  formattedDate = timeClient.getFormattedDate();
  String Year = formattedDate.substring(0, 2);
  String Month = formattedDate.substring(5, 7);
  String Day = formattedDate.substring(8, 10);
  String FileName = "/" + Month + "_" + Day + "_" + Year + "Phase.txt";
  return FileName;
}
String GetFileTime() {
  while (!timeClient.update()) {
    timeClient.forceUpdate();
  }
  formattedDate = timeClient.getFormattedDate();
  String Time = formattedDate.substring(11, 19);

  return Time;
}